# monorepo

[talk: Why Google Stores Billions of Lines of Code in a Single Repository (2016)](https://www.youtube.com/watch?v=W71BTkUbdqE&t=582s)

[talk: Python monorepos: what, why and how (2021)](https://www.youtube.com/watch?v=N6ENyH4_r8U&t=30s)

[talk: The Evolution of our Python Monorepo (2021)](https://www.youtube.com/watch?v=M-RLZHJWHUM)

[tool: pants](https://www.pantsbuild.org/)

[tool: bazel](https://bazel.build/)

[link collection: awesome monorepo](https://github.com/korfuri/awesome-monorepo)

